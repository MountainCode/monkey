// ==UserScript==
// @name         Replace YouTube with invidio.us
// @namespace    http://tampermonkey.net/
// @version      0.1
// @grant        none
// ==/UserScript==

(function() {
  const hostIsYoutube = !!window.location.host.match(/youtube\.com$/);
  const path = window.location.pathname;
  if(hostIsYoutube && path !== '/') {
    window.location = `https://inv.tux.pizza${path}${window.location.search}`;
    return;
  }
  setInterval(function() {
    const as = document.querySelectorAll('a');
    for(const a of document.querySelectorAll('a')) {
      const href = a.getAttribute('href');
      if(!href) { continue; }
      if(hostIsYoutube) {
        a.setAttribute('href', href.replace(/^\/(.*)$/, 'https://inv.tux.pizza/$1'));
      }
      else {
        a.setAttribute('href', href.replace('www.', '').replace('youtube.com', 'inv.tux.pizza').replace('youtu.be', 'inv.tux.pizza'));
      }
    }
  }, 1000);
})();
